from django.db import models

from wagtail.models import Page
from wagtail.fields import StreamField
from wagtail.blocks import RichTextBlock
from wagtail.admin.panels import (
    FieldPanel,
)

from wagtailperson.blocks import (
    HeaderBlock,
    LinkBlock,
    PersonBlock,
)


class HomePage(Page):
    """A simple home page, to test the blocks in a StreamField"""
    body = StreamField(
        [
            ('heading', HeaderBlock()),
            ('link', LinkBlock()),
            ('person', PersonBlock()),
            ('paragraph', RichTextBlock()),
        ],
        blank=True,
        use_json_field=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel('body')
    ]

